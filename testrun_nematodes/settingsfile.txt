demultiplex=TRUE
min=1400
max=2000
quality=15
trim_primers=TRUE
primer_error_rate=0.2
min_abundance=0.0001
cluster_perc=0.97
rank=species
threads=2
count_table=FALSE
group=18S_nem
barcode_file=barcode_files.txt
input_file=test_dataset_16S.fastq
