conda create --name nemmap --file dependency_file.txt && echo conda environment succesfully created!

#setup environment for easy running
sudo mv -fT nemmap.sh /usr/local/bin/nem
sudo mv -fT parse_silva_output_count.py /usr/local/bin/parse_silva_output_count.py
sudo chmod +x /usr/local/bin/nem
sudo chmod +x /usr/local/bin/parse_silva_output_count.py
sudo mkdir /usr/local/share/nemmap
sudo mv -fT 16S_silva_full /usr/local/share/nemmap/
sudo mv -fT 18S_NEM_db /usr/local/share/nemmap/
sudo mv -fT UNITE_ITS_emu /usr/local/share/nemmap/

#cleanup
rm README.md
rm dependency_file.txt
rm nemmap.png
rm LICENSE #not sure if to leave this
rm Nemmap_Diagram.png

