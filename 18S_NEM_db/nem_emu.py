"""Author: Ids Willemsen
Description: Script go generate necessary inputs for emu based on
otal_nematode_Alignment_SSU_8j_secstruc.fas"""

def parse_fasta_speciesname(fastafile, outputfile):
    """Scrapes Total_nematode_Alignment_SSU_8j_secstruc.fas
    for species names, outputs this in a text file"""
    with open(fastafile, "r") as f, open(outputfile, "w") as o :
        for line in f:
            if line.startswith(">"):
                print("TRUE")
                lineofinterest = line.split(" ")[2:4]
                if lineofinterest[0].startswith("Cf.") or lineofinterest[0].startswith("cf."):
                    lineofinterest[0] = lineofinterest[0][3:].lstrip()
                if lineofinterest[1].find("sp.") != -1:
                    lineofinterest[1] = ""
                if lineofinterest[1].find("cf.") != -1:
                    lineofinterest[1] = ""
                speciesname = lineofinterest[0] + " " + lineofinterest[1]
                speciesname = ''.join([char for char in speciesname if not char.isdigit()])
                o.write("{}\n".format(speciesname))
    return None

parse_fasta_speciesname("Total_nematode_Alignment_SSU_8j_secstruc.fas", "nem_names.txt")

def build_dic(tax_id):
    """Creates a dictionary based on NCBI output"""
    dic = {}
    with open(tax_id,"r") as tax:
        f = tax.readlines()[2:]
        for line in f:
            linelist = line.split("|")
            print(linelist)
            species = line.split("|")[1].strip()
            tax = line.split("|")[3].strip()
            dic[species] = tax
    return dic

dic = build_dic('name_tax_ncbi.txt')

def create_tsv(fastafile, tsv, dic):
    """Generates --seq2tax file as input for emu,
    Fastafile should contain Total_nematode_Alignment_SSU_8j_secstruc.fas
    tsv contains the name of the outputfile
    dic should contain a dictionairy of species names to taxIDs"""
    with open(fastafile, "r") as f, open(tsv, "w") as o :
        for line in f:
            if line.startswith(">"):
                lineofinterest = line.split(" ")[2:4]
                if lineofinterest[0].startswith("Cf.") or lineofinterest[0].startswith("cf."):
                    lineofinterest[0] = lineofinterest[0][3:].lstrip()
                if lineofinterest[1].find("sp.") != -1:
                    lineofinterest[1] = ""
                if lineofinterest[1].find("cf.") != -1:
                    lineofinterest[1] = ""
                speciesname = lineofinterest[0] + " " + lineofinterest[1]
                speciesname = ''.join([char for char in speciesname if not char.isdigit()])
                speciesname = speciesname.strip()
                ID = dic[speciesname]
                if ID == "":
                    if speciesname.split(" ")[0] in dic.keys():
                        ID = dic[speciesname.split(" ")[0]]
                if ID == "":
                    ID = "Unknown"
                o.write("{}\t{}\n".format(line.split(" ")[0][1:],ID))
    return None

create_tsv("Total_nematode_Alignment_SSU_8j_secstruc.fas","emu_header_id_6key.tsv",dic)

def cleanupdb(fastain, fastaout):
    """Generates sequence database as input for emu,
    takes Total_nematode_Alignment_SSU_8j_secstruc.fas as input"""
    with open(fastain, "r") as f, open(fastaout, "w") as o:
        for line in f:
            if line.startswith(">"):
                key = line.split(" ")[0]
                o.write('\n')
                o.write('{}\n'.format(key))
            else:
                cleanedline = line.replace("-","").strip()
                o.write(cleanedline)
    return None

if __name__ == "__main__":
    parse_fasta_speciesname("Total_nematode_Alignment_SSU_8j_secstruc.fas", "nem_names.txt")
    #after parse_fasta_speciesname the NCBI tool should be used to create name_tax_ncbi.txt
    dic = build_dic('name_tax_ncbi.txt')
    create_tsv("Total_nematode_Alignment_SSU_8j_secstruc.fas", "emu_header_id_6key.tsv", dic)
    cleanupdb("Total_nematode_Alignment_SSU_8j_secstruc.fas", 'nem_reads.fasta')