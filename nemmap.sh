#!/bin/bash	

#version=0.1.2

#NEmatology MinION Microbial Analysis Pipeline

#check if the first argument given is the settingsfile
if [[ -f "$1" ]]; then
    SETTINGS_FILE=$1
else
    echo "Please provide a valid settings file."
    exit 1
fi

#parse the settingsfile for key arguments
while IFS='=' read -r key value; do
    # Ignore lines starting with '#' (comments) and empty lines
    if [[ ! "$key" =~ ^# && "$key" != "" ]]; then
        # Export variables as environment variables (or just use eval to assign)
        eval "$key=\"$value\""
    fi
done < "$SETTINGS_FILE"

#check which options are specified and set a variable with the specified value. 
while [ $# -gt 0 ]
do
	case "$1" in 
	
		-demultiplex)
			demultiplex=$2
			;;
			
		-min)
			min=$2
		;;
		
		-max)
			max=$2
		;;
		
		-quality)
			quality=$2
		
		;;
		
		-trim_primers)
			trim=$2
		;;
		
		-primer_error_rate)
			error=$2
		;;
		
		-chimera_filtering)
			chimera=$2
		;;
		
		-cluster_perc)
			cluster_perc=$2
			
		;;
		
		-group)
			group=$2
		;;
		
		-rank)
			rank=$2
		;;
		
		-min_abundance)
			min_abundance=$2
		;;
		
		-threads)
			threads=$2
		;;
		
		-barcode_file)
			barcode_file=$2
		;;
		
		-input_file)
			input_file=$2
		;;
		
		-count_table)
			count_table=$2
		;;
		
		*) 
			echo Error! Unrecognized argument detected. Please check your spelling and the correct usage below.
		;;

	esac
	
	shift
	shift

done


#below, some conditions of the options are evaluated.

if [ -z ${group} ]
then
	echo group 16S_bac, 18S_nem or ITS_fungi not specified, please do so.
	
	exit 1
fi

#if [ ${group} ]#is not 16S or ITS.. 
#then
#	echo Error! The group you specify should be either "16S_bac" or "ITS_fungi". 
#	exit 1
#fi

if [ -z ${input_file} ] && [ ${demultiplex} == "TRUE" ]
then
	echo You did not specify an input file but this is required. Please do so.
	exit 1
fi

#if [ ! -f ${input_file} ]
#then
#	echo Error! Your input is not a file.
#	exit 1
#fi

if [ ${chimera} == "TRUE" ]
then

	echo chimera filtering is requested, but this is not possible for now. Exiting pipeline and please remove this option.
	
	exit 1
fi

#move the input calls to an own directory

#demultiplex the file
if [ ${demultiplex} == "TRUE" ] 
then

	mkdir input_data/
	mv ${input_file} input_data/

	echo Demultiplexing reads!

	dorado demux \
		--kit-name EXP-NBD196 \
		--emit-fastq \
		--output-dir demultiplexed/ \
		input_data/${input_file}

#rename files: remove EXP-NBD196_. This gives a warning	"mv: 'demultiplexed/unclassified.fastq' and 'demultiplexed/unclassified.fastq' are the same file" which can be ignored
	cd demultiplexed
	ls *.fastq | grep -vFf ../${barcode_file} | xargs rm #remove unused barcodes
	cd ..

	for file in demultiplexed/*.fastq;
	do
		mv "$file" "${file/EXP-NBD196_/}"
	done 


#old rename command that failed at other PC's
#rename 's/EXP-NBD196_//' demultiplexed/*.fastq
	
	
	
elif [ ${demultiplex} == "FALSE" ]
then
		echo You specified 'demultiplex=FALSE'. Now the pipeline assumes that your data is already demultiplexed and that all demultiplexed files are within a directory/folder named demultiplexed.
		
		echo type 'yes' below if this indeed is the case. Type 'no' if you would like to exit the pipeline and reorganise your files.
		
		read verification
		
		
		if [ ${verification} == "yes" ]
		then 
			
			echo you verified that your files are in the correct structure, great! 
			
		elif [ ${verification} == "no" ]
		then
			
			echo you recognized that your files are not in the correct structure yet. The script will now exit and you can reorganise the files.
			
			exit 1
		
		fi	
		
fi
  
#loop over barcoded files and filter using NanoFilt

cd demultiplexed/

for file in *.fastq 
do

	echo filtering length and quality of ${file}
	
	NanoFilt --length ${min} --maxlength ${max} -q ${quality} ${file} > filtered_${file}
	
done

echo Filtering of files complete

cd ..

#move filtered files to new directory for structure.
mkdir filtered_files/

mv demultiplexed/filtered* filtered_files/ #I assume here that the filtered files are saved in the head directory.


#trim the primers using cutadapt
if [ ${trim_primers} == "TRUE" ] 
then

	if [ "${group}" = "16S_bac" ] ; then
	
	cd filtered_files/
		
		for filtered_file in *.fastq 
		do
		
			echo trimming primers from ${filtered_file}
			
			cutadapt --error-rate ${primer_error_rate} --match-read-wildcards --revcomp --cores ${threads} --front AGAGTTTGATCMTGGCTCAG --front CGGTTACCTTGTTACGACTT --adapter CTGAGCCAKGATCAAACTCT --adapter AAGTCGTAACAAGGTAACCG --times 2 --quiet ${filtered_file} > trimmed_${filtered_file}
		
		done
		
		echo primers trimmed from all barcodes 
		
		cd ..
		
		mkdir trimmed_files/
		
		mv filtered_files/trimmed*.fastq trimmed_files/ #all filtered + trimmed files should now be in this folder.
		
	elif [ ${group} == "18S_nem" ]; then
	
	cd filtered_files/
		
		for filtered_file in *.fastq 
		do
		
			echo trimming primers from ${filtered_file}
						
			cutadapt --error-rate ${primer_error_rate} --match-read-wildcards --revcomp --cores ${threads} --front ctcaaagattaagccatgc --front aaaagtcgtaacaaggtagc --adapter gctaccttgttacgactttt --adapter gcatggcttaatctttgag --times 2 --quiet ${filtered_file} > trimmed_${filtered_file}

		done
		
		echo primers trimmed from all barcodes 
		
		cd ..
		
		mkdir trimmed_files/
		
		mv filtered_files/trimmed*.fastq trimmed_files/ 





	elif [ ${group} == "ITS_fungi" ] 
	
	then
	
		echo fungal ITS sequences do not require cutadapt to trim primers. Switching to ITSxpress for ITS region extraction.
	
	fi
		

elif [ ${trim_primers} == "FALSE" ]
then	
	if [ "${group}" = "16S_bac" ] || [ "${group}" = "18S_nem" ]; then
		echo "for now, the pipeline requires the trim_primers argument to be TRUE for bacterial 16S data or nematode 18S data. This will change in the future."
	
		exit 1
	fi 
	
	elif [ ${group} == "ITS_fungi" ] 
	then
		
		echo Switching to ITSxpress for ITS region extraction.
	
	

fi
		
		
if [ ${group} == "ITS_fungi" ]
	
then
	echo using ITSxpress to extract fungal ITS regions.
	
	cd filtered_files	
		
	for filtered_file in *.fastq 	
	do
		itsxpress --fastq ${filtered_file} --single_end --outfile ITS_select_${filtered_file} --region ALL --threads ${threads}
	done
	
	cd ..
	
	mkdir ITS_selected/
	
	mv filtered_files/ITS_select_* ITS_selected/
	
fi

#cluster the reads using vsearch

mkdir emu_input #first make a directory where the input for emu will be located, either clustered or non-clustered reads.

if [ ${cluster_perc} != 'FALSE' ]
then
	if [ "${group}" = "16S_bac" ] || [ "${group}" = "18S_nem" ]; then
	
		cd trimmed_files
	
		echo clustering reads on ${cluster_perc} identity using vsearch
	
		for trimmed_file in trimmed_*.fastq
		do
	
			echo clustering ${trimmed_file}
	
			vsearch --cluster_fast ${trimmed_file} \
			--id ${cluster_perc} \
			--sizeout \
			--threads ${threads} \
			--consout clustered_${trimmed_file}.fasta
		done	
	
		echo finished clustering all 16S/18S files
		
		echo rereplicating all clustered 16S/18S consensus sequences for emu 
		
		for clustered_file in clustered_*.fasta
		do
		
			echo rereplicating ${clustered_file}
		
			vsearch --rereplicate ${clustered_file} \
			--relabel sequence \
			--output rereplicated_${clustered_file}
		
		done
		
		
		
		cd ..
	
		mv ./trimmed_files/rereplicated_*.fasta ./emu_input/
	
	elif [ ${group} == "ITS_fungi" ]
	then
	
		cd ITS_selected
	
		echo clustering reads on ${cluster_perc} identity using vsearch
	
		for ITS_file in ITS_select_*.fastq
		do
		
			echo clustering ${ITS_file}
		
			vsearch --cluster_fast ${ITS_file} \
			--id ${cluster_perc} \
			--sizeout \
			--threads ${threads} \
			--consout clustered_${ITS_file}.fasta 
		done
		
		echo finished clustering all ITS trimmed files
		
		echo rereplicating all clustered ITS consensus sequences for emu 
		
		for clustered_file in clustered_*.fasta
		do
		
			echo rereplicating ${clustered_file}
		
			vsearch --rereplicate ${clustered_file} \
			--relabel sequence \
			--output rereplicated_${clustered_file}
		
		done
		
		
		cd ..
		
		mv ./ITS_selected/rereplicated_*.fasta ./emu_input/
	fi
	
	
elif [ ${cluster_perc} == 'FALSE' ] 
then	
	if [ "${group}" = "16S_bac" ] || [ "${group}" = "18S_nem" ]; then
	
		echo 16S/18S reads are not clustered, moving primer trimmed reads to the emu_input folder
		
		mv trimmed_files/trimmed_*.fastq ./emu_input/
	elif [ ${group} == "ITS_fungi" ]
	then
		
		echo ITS reads are not clustered, moving ITS extracted reads to the emu_input folder
		
		mv ITS_selected/ITS_select_*.fastq ./emu_input/
	
	fi
	
fi	


#here your directory is the 'main' directory

#classify the reads using emu
if [ ${group} == "16S_bac" ]
then
#do emu on all samples
#check if the strand orientation matters for emu!!
	mkdir all_emu_results_16S/
	
	cd emu_input	
	
	for file in *
	do
		echo classifying reads of ${file}

		emu abundance ${file} \
			--db /usr/local/share/nemmap/16S_silva_full/ \
			--keep-counts \
			--min-abundance ${min_abundance} \
			--type map-ont \
			--threads ${threads} 
			
	done

	
	mv results/*_rel-abundance.tsv ../all_emu_results_16S/
				
	cd ..
	
	echo merging results files to a single file.
	
	if [ ${count_table} == "TRUE" ]
	then
	
		parse_silva_output_count.py -d all_emu_results_16S/ -c
		
		echo Printing read counts in OTU table
		
	elif [ ${count_table} == "FALSE" ]
	then
		parse_silva_output_count.py -d all_emu_results_16S/ 
		
		echo Printing relative abundances in OTU table
	
	fi
	
elif [ ${group} == "18S_nem" ]
then
#do emu on all bacodes
#check if the strand orientation matters for emu!!
	mkdir all_emu_results_18S/
	
	cd emu_input	
	
	for file in *
	do
		echo classifying reads of ${file}

		emu abundance ${file} \
			--db /usr/local/share/nemmap/18S_NEM_db/ \
			--keep-counts \
			--min-abundance ${min_abundance} \
			--type map-ont \
			--threads ${threads} 
			
	done

	
	mv results/*_rel-abundance.tsv ../all_emu_results_18S/
				
	cd ..
	
	echo merging results files to a single file.
	
	if [ ${count_table} == "TRUE" ]
	then
	
		emu combine-outputs ./all_emu_results_18S species --counts
		
		echo Printing read counts in OTU table
		
	elif [ ${count_table} == "FALSE" ]
	then
		emu combine-outputs ./all_emu_results_18S species
		
		echo Printing relative abundances in OTU table
	
	fi
	
elif [ ${group} == "ITS_fungi" ]
then
	mkdir all_emu_results_ITS
	
	cd emu_input
	
	for file in *
	do
		echo classifying reads of ${file}

		emu abundance ${file} \
			--db /usr/local/share/nemmap/UNITE_ITS_emu/  \
			--keep-counts \
			--min-abundance ${min_abundance} \
			--type map-ont \
			--threads ${threads} 
			
	done
	
	mv results/*_rel-abundance.tsv ../all_emu_results_ITS/
	
	cd ..
	
	#remove barcode files that are NOT in the barcodes_file.txt file
	
	
	#merge all barcode files together
	parse_silva_output.py -d all_emu_results_ITS/
fi

echo NEMMAP pipeline completed! You can find your final results in the all_emu_results folder.

#if [ ${group} == "16S_bac" ]
#then
	#echo NEMMAP pipeline completed! You can find your final results in the all_emu_results_16S folder.
	
#elif [ ${group} == " ITS_fungi" ]
#then
	#echo NEMMAP pipeline completed! You can find your final results in the all_emu_results_ITS folder.

#fi


##===========================================INACTIVE FOR NOW=====================================#
##chimera filtering will be completed later on
## if [ chimera == "TRUE" ]
## then
## #perform chimera filtering
#	# for barcode in barcode*; do

#		# cd $barcode

#		# echo converting $barcode to .fasta
#		# seqtk seq -a all_trimmed_$barcode.fastq > all_trimmed_$barcode.fasta

## #this removes ~30% of the reads, which is likely not correct. Likely vsearch does not work well with nanopore data... does strand orientation play a role here?
## #if we use cutadapt to get the same orientation for all strands --> does this work better?
#		# echo removing chimeras for $barcode
#		# vsearch --uchime_ref all_trimmed_$barcode.fasta \
#			# --quiet \
#			# --db /home/thebiobeast/Documents/Robbert/MinION/refDBs/16S_silva_full/species_taxid.fasta \
#			# --nonchimeras \
#			# all_trimmed_$barcode.nonchim.fasta 

#		# cd ..
#	# done

## fi